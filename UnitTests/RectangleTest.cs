﻿using System.Drawing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rectangle = EpamITLab.Task2.Rectangle;

namespace EpamITLab.UnitTests
{
    [TestClass]
    public class RectangleTest
    {
        [TestMethod]
        public void CanCalculateTheCoordinatsOfThePoint()
        {
            const int x = 100, y = 200, width = 300, height = 200;
            var rectangle = new Rectangle(x, y, width, height);

            Assert.IsNotNull(rectangle);
            Assert.AreEqual(new Point(x, y), rectangle[0]);
            Assert.AreEqual(new Point(x + width, y), rectangle[1]);
            Assert.AreEqual(new Point(x + width, y - height), rectangle[2]);
            Assert.AreEqual(new Point(x, y - height), rectangle[3]);
        }

        [TestMethod]
        public void CanMoveRectangle()
        {
            const int x = -100, y = -200, width = 300, height = 200;
            var rectangle = new Rectangle(x, y, width, height);

            const int newX = 150, newY = -500;
            rectangle.MoveTo(new Point(newX, newY));

            Assert.AreEqual(new Point(150, -500), rectangle[0] );
            Assert.AreEqual(new Point(newX + width, newY), rectangle[1]);
            Assert.AreEqual(new Point(newX + width, newY - height), rectangle[2]);
            Assert.AreEqual(new Point(newX, newY - height), rectangle[3]);
        }

        [TestMethod]
        public void CanResizeRectangle()
        {
            const int x = -50, y = 50, width = 100, height = 50;
            var rectangle = new Rectangle(x, y, width, height);

            rectangle.Resize(200, 20);

            Assert.AreEqual(200, rectangle.Width);
            Assert.AreEqual(20, rectangle.Height);
            Assert.AreEqual(new Point(-50, 50), rectangle[0]);
            Assert.AreEqual(new Point(150, 50), rectangle[1]);
            Assert.AreEqual(new Point(150, 30), rectangle[2]);
            Assert.AreEqual(new Point(-50, 30), rectangle[3]);
        }

        [TestMethod]
        public void CanCreateIntersectionOfRectangles()
        {
            var rect1 = new Rectangle(0, 0, 10, 10);
            var rect2 = new Rectangle(0, 0, 20, 20);

            var intersection = Rectangle.Intersect(rect1, rect2);

            Assert.IsNotNull(intersection);
            Assert.AreEqual(new Point(0, 0), intersection[0]);
            Assert.AreEqual(new Point(10, 0), intersection[1]);
            Assert.AreEqual(new Point(10, -10), intersection[2]);
            Assert.AreEqual(new Point(0, -10), intersection[3]);
        }
    }
}
